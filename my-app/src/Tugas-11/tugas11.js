import React, { Component } from 'react'

class Timer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      time: 100
    }
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  };

  tick() {
    this.setState({
      time: this.state.time - 1
    });
  };


  render() {
    let d = new Date();
    let jam = d.getHours() > 12 ? d.getHours() - 12 : d.getHours();
    let tanda = d.getHours() > 0 && d.getHours() < 13 ? 'AM' : 'PM';
    if (this.state.time > 0) {
      return (
        <div>
          <h1 style={{ textAlign: "center" }}>
            {`sekarang jam : ${jam}:${d.getMinutes()}:${d.getSeconds()} ${tanda}`}
          </h1>
          <h1 style={{ textAlign: "center" }}>
            {`hitung mundur : ${this.state.time}`}
          </h1>
        </div>
      );
    } else {
      return (
        null
      )
    }
  };
};

export default Timer;