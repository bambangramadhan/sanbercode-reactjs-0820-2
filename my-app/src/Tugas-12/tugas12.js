import React, { Component } from "react";
import './tugas12.css'

class Lists extends Component {

  constructor(props) {
    super(props)
    this.state = {
      buah: [
        {
          nama: 'Semangka',
          harga: 10000,
          berat: '1 kg'
        },
        {
          nama: 'Anggur',
          harga: 40000,
          berat: '0.5 kg'
        },
        {
          nama: 'Strawberry',
          harga: 30000,
          berat: '0.4 kg'
        },
        {
          nama: 'Jeruk',
          harga: 30000,
          berat: '1 kg'
        },
        {
          nama: 'Mangga',
          harga: 30000,
          berat: '0.5 kg'
        }
      ],
      inputName: "",
      inputHarga: 0,
      inputBerat: '0 kg',
      editable: false,
      index: 0
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    console.log(event);
    this.setState({
      inputName: event.target.nama,
      inputHarga: event.target.harga,
      inputBerat: event.target.berat
    });
  }

  handleSubmit(event) {
    event.preventDefault()
    this.setState({
      buah: [...this.state.buah, this.state.inputName],
      inputName: ""
    })
  }

  render() {
    return (
      <div style={{ margin: '0 auto', width: ' 70vw' }}>
        <h1 style={{ textAlign: 'center' }}>Tabel Harga Buah</h1>
        <table>
          <thead>
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Aksi</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {
              this.state.buah.map((val, index) => {
                if (this.state.editable && index === this.state.index) {
                  return (
                    <tr>
                      <td><input type="text" name="nama" value={this.state.inputName} onChange={this.handleChange} /></td>
                      <td><input type="text" name="harga" value={this.state.inputHarga} onChange={this.handleChange} /></td>
                      <td><input type="text" name="berat" value={this.state.inputBerat} onChange={this.handleChange} /></td>
                      <td><button onSubmit={this.handleSubmit}>Submit</button></td>
                    </tr>
                  )
                }
                return (
                  <tr>
                    <td>{val.nama}</td>
                    <td>{val.harga}</td>
                    <td>{val.berat}</td>
                    <td><button style={{ margin: 5 }} onClick={() => this.setState({ editable: true })}>Edit</button> <button>Delete</button></td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
        {/* Form */}
        <h1>Form Buah</h1>
        <form onSubmit={this.handleSubmit}>
          <label>
            Masukkan data buah:
          </label>
          <input type="text" name="nama" value={this.state.inputName} onChange={this.handleChange} />
          <input type="text" name="harga" value={this.state.inputHarga} onChange={this.handleChange} />
          <input type="text" name="berat" value={this.state.inputBerat} onChange={this.handleChange} />
          <button>submit</button>
        </form>
      </div>
    )
  }
}

export default Lists